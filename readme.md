# VRChat Emoji Manager

VRChat Plus has a custom emoji feature but it is limited to 5 emojis and you cannot change their animation styles. This Chrome extension allows you to have a much larger collection of emojis and conveniently toggle them on and off when needed.

![image](2023-10-26_20-50-20-123%20VRChat_Emoji_Manager_-_Google_Chrome.png)

## Install

Is it worth the $5 to publish this to Chrome Web Store??

1. [Download](https://gitea.moe/lamp/vrchat-emoji-manager/archive/main.zip) and unzip repository
2. Navigate to [chrome://extensions/](chrome://extensions/)
3. Enable developer mode
4. Click "Load unpacked"
5. Select the unzipped folder
6. Open https://vrchat.com/home, existing emojis should be imported
7. Click extension icon to launch in tab or window

IF ANY ISSUES REPORT HERE