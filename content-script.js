fetch("https://vrchat.com/api/1/files?tag=emoji&n=100&offset=0").then(res => res.json()).then(data => {
	if (!data.error)
		chrome.runtime.sendMessage(["storeEmojis", data]);
});


var functions = {
	async getEmojis() {
		return await fetch("https://vrchat.com/api/1/files?tag=emoji&n=100&offset=0").then(res => res.json());
	},
	async deleteEmoji(id) {
		return await fetch(`https://vrchat.com/api/1/file/${id}`, {method: "DELETE"}).then(res => res.json());
	},
	async createEmoji(url, animationStyle) {
		var blob = await fetch(url).then(res => res.blob());
		var form = new FormData();
		form.append("tag", "emoji");
		form.append("animationStyle", animationStyle);
		form.append("maskTag", "square");
		form.append("file", blob);
		return await fetch("https://vrchat.com/api/1/file/image", {
			method: "POST",
			body: form
		}).then(res => res.json());
	}
};

chrome.runtime.onMessage.addListener(function([method, ...args], sender, sendResponse) {
	console.debug(arguments);
	functions[method]?.apply(null, args).then(sendResponse).catch(error => sendResponse({error: error.toString()}));
	return true;
});