
var functions = {
	async storeEmojis(emojis) {
		console.debug("storeEmojis", emojis);
		var emojiStore = (await chrome.storage.local.get(["emojis"])).emojis || [];

		loop1: for (let emoji of emojis) {
			for (let existingEmoji of emojiStore) {
				if (existingEmoji.currentId == emoji.id) continue loop1;
			}
			console.log("store", emoji.id);
			var blob = await fetch(emoji.versions[1].file.url).then(res => res.blob());
			var dataString = await blobToDataURL(blob);
			var emojiDoc = {
				date: emoji.versions[1].created_at,
				currentId: emoji.id,
				internalId: emoji.id,
				animationStyle: emoji.animationStyle
			};
			await chrome.storage.local.set({[`data-${emojiDoc.internalId}`]: dataString});
			emojiStore.push(emojiDoc);
		}

		chrome.storage.local.set({emojis: emojiStore});
	}
};



chrome.runtime.onMessage.addListener(function([method, ...args], sender, sendResponse) {
	console.debug(arguments);
	functions[method]?.apply(null, args).then(sendResponse).catch(error => sendResponse({error: error.toString()}));;
	return true;
});






function blobToDataURL(blob) {
	return new Promise((resolve, reject) => {
		var reader = new FileReader();
		reader.readAsDataURL(blob);
		reader.onload = () => {
			resolve(reader.result);
		};
		reader.onerror = reject;
	});
}